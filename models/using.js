'use strict';
module.exports = (sequelize, DataTypes) => {
    var Using = sequelize.define('using', {
        id: {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true}

    }, {timestamps: false});

    /*
      Using.associate = function(models) {
        models.using.hasMany(models.transactions);
      };

      */
    return Using;
};
