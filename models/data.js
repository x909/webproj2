'use strict';
module.exports = (sequelize, DataTypes) => {
    var Data = sequelize.define('data', {
        id: {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
        date_start: {type: DataTypes.DATE, validate: {isDate: true}},
        date_end: {type: DataTypes.DATE, validate: {isDate: true}},
        value: DataTypes.DECIMAL,
    }, {timestamps: false});


    Data.associate = function (models) {
        models.data.belongsToMany(models.transactions, {through: {model: models.transmapsdata}, onDelete: 'cascade'});
        models.data.belongsTo(models.sources);
    };

    return Data;
};
