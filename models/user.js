'use strict';
module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('users', {
        id: {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
        email: {type: DataTypes.STRING, validate: {isEmail: true}},
        name: {type: DataTypes.STRING, unique: true},
        password: {
            type: DataTypes.STRING, allowNull: false,
            validate: {
                len: {args: [8, Infinity], msg: "Password must be at least 8 characters."},
            }

        }
    }, {timestamps: false});

    User.associate = function (models) {
        models.users.hasMany(models.transactions, {as: 'transactions'});
  };

  return User;
};
