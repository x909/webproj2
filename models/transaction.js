var models = require('../models');

'use strict';
module.exports = (sequelize, DataTypes) => {
    var Transaction = sequelize.define('transactions', {
            id: {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
            date_start: {type: DataTypes.DATE, validate: {isDate: true}},
            date_end: {type: DataTypes.DATE, validate: {isDate: true}},
            value: DataTypes.DECIMAL,


        }, {timestamps: false}
    );


    Transaction.associate = function(models) {
        console.log(models);
        models.transactions.belongsToMany(models.data, {through: {model: models.transmapsdata}, onDelete: 'cascade'});
        models.transactions.belongsTo(models.users, {onDelete: 'cascade'});
        models.transactions.belongsTo(models.sources);
    };


    return Transaction;
};
