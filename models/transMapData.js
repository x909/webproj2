'use strict';
module.exports = (sequelize, DataTypes) => {
    var Maps = sequelize.define('transmapsdata', {
        id: {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
        value: {type: DataTypes.DECIMAL},
    }, {timestamps: false});


    return Maps;
};
