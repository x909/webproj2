'use strict';
module.exports = (sequelize, DataTypes) => {
    var Source = sequelize.define('sources', {
        id: {type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true},
        description: DataTypes.STRING,
        name: DataTypes.STRING,
    }, {timestamps: false});

    Source.associate = function (models) {
        models.sources.hasMany(models.data);
        models.sources.hasMany(models.transactions);
    };


    return Source;
};
