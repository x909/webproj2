var models = require('../models');
var express = require('express');
var router = express.Router();

router.post('/', function (req, res) {
    models.sources.findAll({
        include: [models.data]
    }).then(function (sources) {
        res.json(sources);
    });
});

router.put('/', function (req, res) {
    models.sources.findAll({
        include: [models.data]
    }).then(function (sources) {
        res.json(sources);
    });
});

router.get('/', function (req, res) {
    models.sources.findAll({
        include: [models.data]
    }).then(function (sources) {
        res.json(sources);
    });
});

router.delete('/', function (req, res) {
    models.sources.findAll({
        include: [models.data]
    }).then(function (sources) {
        res.json(sources);
    });
});


module.exports = router;
