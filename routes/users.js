var models  = require('../models');
var express = require('express');
var router  = express.Router();

router.post('/register', function(req, res) {

  var email = req.body.email;
  var name = req.body.name;
  var password = req.body.password;

    console.log(req.body);
  if ( !email || !name || !password) {
      res.status(400);
      res.send({Bullshit: true});
      return;
  }

    models.users.create({
    email: req.body.email,
    name: req.body.name,
    password: req.body.password

  }).then(function() {
        res.send({
            error: false, error_type: "" +
                "" +
                "", err_msg: "OK!"
        });
  }).catch ( err =>  {
        res.status(400);
        res.send({error: true, error_type: err.name, err_msg: err.message});
    }
  );

});

router.post('/login', function(req, res) {

    var name = req.body.name;
    var password = req.body.password;

    if (!name || !password) {
        res.status(400);
        res.send({Bullshit:true});
    }

    models.users.findAndCount({
    where: {
      name: req.body.name, password: req.body.password
    }
  }).then(result => {

      if ((result.count) !== 1) {
          throw new Error("The username or password is not correct (maybe both?)");
      } else {

          console.log(result.rows[0].dataValues);
          res.send({
              error: false,
              error_type: "",
              err_msg: "OK!",
              data: [{
                  user_id: result.rows[0].dataValues.id,
                  email: result.rows[0].dataValues.email,
                  name: result.rows[0].dataValues.name
              }]
          });


      }
  }).catch ( err =>  {
            res.status(400);
        res.send({error: true, error_type: err.name, err_msg: err.message});

        }
    );
});



module.exports = router;
