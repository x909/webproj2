var models = require('../models');
var express = require('express');
var router = express.Router();


router.get('/sum/:user_id/:source_id/', function (req, res) {

    var user_id = req.params.user_id;
    var source_id = req.params.source_id;

    if (!user_id || !source_id) {
        res.status(400);
        res.send({Bullshit: true});
    }

    models.transactions.findAll({
        where: {
            userId: user_id,
            sourceId: source_id,

        }, required: true,

        include: [
            {model: models.data, required: true}
        ]

    }).then(result => {

        var accumulate_sum;
        var date_end;
        var map_date_total_sum = {};
        //
        for (let o of result) {
            // console.log(o.data[0].transmapsdata);
            date_end = o.date_end;
            accumulate_sum = 0;
            for (let i of o.data) {
                //data
                accumulate_sum += i.transmapsdata.value;
            }
            map_date_total_sum[date_end.toISOString()] = accumulate_sum;

        }
        console.log(map_date_total_sum);
        res.send({error: false, error_type: "", err_msg: "OK!", data: [map_date_total_sum]});
    }).catch(err => {
        res.status(400);
        res.send({error: true, error_type: err.name, err_msg: err.message});
    });


});

router.get('/diff/:user_id/:source_id/', function (req, res) {

    var user_id = req.params.user_id;
    var source_id = req.params.source_id;

    if (!user_id || !source_id) {
        res.status(400);
        res.send({Bullshit: true});
    }

    models.transactions.findAll({
        where: {
            userId: user_id,
            sourceId: source_id,

        },
        required: true,
        order: [['date_end', 'ASC']],

        include: [
            {model: models.data, required: true, order: [['date_end', 'ASC']]}
        ],


    }).then(result => {

        var diff = 0;
        var sum_1 = 0;
        var sum_2 = 0;
        var diff_date;
        var date_end_2;
        var date_end_1;
        var diff_sum = 0;
        var map_date_total_sum = {};
        //

        if (result) {

            date_end_1 = result[0].date_end;

            for (let i of result[0].data) {

                sum_1 += i.transmapsdata.value;

            }
            for (let i = 1; i < result.length; i++) {

                // console.log(o.data[0].transmapsdata);
                date_end_2 = result[i].date_end;
                sum_2 = 0;
                for (let j of result[i].data) {
                    //data
                    sum_2 += j.transmapsdata.value;
                }

                diff_date = Math.ceil((date_end_2.getDate() - date_end_1.getDate()) / 1000 * 3600 * 24);
                diff_sum = sum_2 - sum_1;

                if (diff_date === 0) {
                    continue;
                }


                diff = diff_sum / diff_date;

                map_date_total_sum[date_end_2.toISOString()] = (diff);

                date_end_1 = date_end_2;
                sum_1 = sum_2;


            }
        }
        res.send({error: false, error_type: "", err_msg: "OK!", data: [map_date_total_sum]});
    }).catch(err => {
        res.status(400);
        res.send({error: true, error_type: err.name, err_msg: err.message});
    });


});


module.exports = router;
