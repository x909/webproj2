var models = require('../models');
var express = require('express');
var router = express.Router();


models.transactions.afterCreate((transactions, options) => {

    transactions.date_end.setDate(transactions.date_end.getDate() + 1);

    models.data.findAll({
        where: {
            sourceId: transactions.sourceId,
            date_start: {
                [sequelize.Op.gte]: transactions.date_start
            },
            date_end: {
                [sequelize.Op.lt]: transactions.date_end
            }
        }
    }).then(result => {

        for (let o of result) {
            transactions.setData([o.id], {through: {value: o.value * transactions.value}});
        }
    });
});

models.transactions.afterDestroy((transactions, options) => {

    console.log("pokemon");
    console.log(transactions);
    console.log(transactions.transmapsdata);
});



router.post('/', function (req, res) {

    var user_id = req.body.user_id;
    var source_id = req.body.source_id;
    var date_start = req.body.date_start;
    var date_end = req.body.date_end;
    var value = req.body.value;

    if (!date_start || !date_end || !value || !user_id || !source_id) {
        res.status(400);
        res.send({Bullshit: true});
        return
    }


    models.transactions.create({
        sourceId: source_id,
        userId: user_id,
        date_start: date_start,
        date_end: date_end,
        value: value,

    }).then(result => {
        res.send({error: false, error_type: "", err_msg: "OK!"});
    }).catch(err => {
        console.log(err);
        res.send({error: true, error_type: err.name, err_msg: err.message});
        }
    );


});

router.put('/:user_id/:source_id/:transaction_id', function (req, res) {

    var user_id = req.params.user_id;
    var transaction_id = req.params.transaction_id;
    var source_id = req.params.source_id;

    var value = req.body.value;

    if (!user_id || !source_id || !transaction_id || !value) {
        res.status(400);
        res.send({Bullshit: true});
        return
    }

    models.transactions.update(
        {value: value},
        {where: {id: transaction_id, userId: user_id, sourceId: source_id}}
    ).then(function () {
        res.send({error: false, error_type: "", err_msg: "OK!"});
    }).catch(err => {
            res.send({error: true, error_type: err.name, err_msg: err.message});
        }
    );

});


router.get('/:user_id/:source_id/:transaction_id', function (req, res) {

    var user_id = req.params.user_id;
    var transaction_id = req.params.transaction_id;
    var source_id = req.params.source_id;

    if (!user_id || !source_id || !transaction_id) {
        res.status(400);
        res.send({Bullshit: true});
    }

    models.transactions.findAll({
        where: {
            userId: user_id, id: transaction_id, sourceId: source_id
        }
    }).then(result => {

        if ((result.length) === 0) {
            res.send({error: false, error_type: "", err_msg: "OK!", data: [{}]});
        } else {
            //should be only one
            res.send({error: false, error_type: "", err_msg: "OK!", data: result[0].dataValues});
        }
    }).catch(err => {
            res.send({
                error: true,
                error_type: err.name,
                err_msg: err.message
            });
        }
    );
});

router.get('/:source_id/:user_id', function (req, res) {

    var user_id = req.params.user_id;
    var source_id = req.params.source_id;

    if (!user_id || !source_id) {
        res.status(400);
        res.send({Bullshit: true});
    }

    models.transactions.findAll({
        where: {
            userId: user_id, sourceId: source_id
        }
    }).then(result => {

        if ((result.length) === 0) {
            res.send({error: false, error_type: "", err_msg: "OK!", data: [{}]});
        } else {
            //should be only one
            res.send({error: false, error_type: "", err_msg: "OK!", data: result});
        }
    }).catch(err => {
            res.send({
                error: true,
                error_type: err.name,
                err_msg: err.message
            });
        }
    );
});


router.delete('/:user_id/:source_id/:transaction_id', function (req, res) {

    var user_id = req.params.user_id;
    var transaction_id = req.params.transaction_id;
    var source_id = req.params.source_id;

    if (!user_id || !transaction_id || !source_id) {
        res.status(400);
        res.send({Bullshit: true});
    }

    models.transactions.destroy({
        where: {
            userId: user_id, id: transaction_id, sourceId: source_id
        }
    }).then(function () {
        res.status(200);
        res.send("");

    }).catch(err => {
            res.status(404);
            res.send({
                error: true,
                error_type: err.name,
                err_msg: err.message
            });
        }
    );
});


module.exports = router;
