var models = require('../models');
var express = require('express');
var router = express.Router();


router.post('/', function (req, res) {

    var source_id = req.body.source_id;
    var date_start = req.body.date_start;
    var date_end = req.body.date_end;
    var value = req.body.value;


    if (!date_start || !date_end || !value || !source_id) {
        res.status(400);
        res.send({Bullshit: true});
        return
    }

    models.data.create({
        sourceId: source_id,
        date_start: date_start,
        date_end: date_end,
        value: value,

    }).then(function () {
        res.send({error: false, error_type: "", err_msg: "OK!"});
    }).catch(err => {
            res.send({error: true, error_type: err.name, err_msg: err.message});
        }
    );

});

router.put('/:source_id/:data_id', function (req, res) {

    var source_id = req.params.user_id;
    var data_id = req.params.data_id;

    var value = req.body.value;

    if (!source_id || !data_id || !value) {
        res.status(400);
        res.send({Bullshit: true});
        return
    }

    models.data.update(
        {value: value},
        {where: {id: transaction_id, sourceId: source_id}}
    ).then(function () {
        res.send({error: false, error_type: "", err_msg: "OK!"});
    }).catch(err => {
            res.send({error: true, error_type: err.name, err_msg: err.message});
        }
    );

});


router.get('/g/:source_id/', function (req, res) {

    var source_id = req.params.source_id;
    console.log(source_id);

    if (!source_id) {
        res.status(400);
        res.send({Bullshit: true});
    }

    models.data.findAll({
        where: {
            sourceId: source_id
        },


    }).then(result => {

        var data_val = {};

        result.forEach(function (data) {


            data_val[data.date_start.toISOString()] = data.value;


        });


        res.send({error: false, error_type: "", err_msg: "OK!", data: [data_val]});

    }).catch(err => {
            res.send({
                error: true,
                error_type: err.name,
                err_msg: err.message
            });
        }
    );
});



router.get('/:source_id/:data_id', function (req, res) {

    var source_id = req.params.source_id;
    var data_id = req.params.data_id;

    if (!source_id || !data_id) {
        res.status(400);
        res.send({Bullshit: true});
    }

    models.data.findAll({
        where: {
            sourceId: source_id, id: data_id
        }
    }).then(result => {

        if ((result.length) === 0) {
            res.send({error: false, error_type: "", err_msg: "OK!", data: [{}]});
        } else {
            //should be only one
            res.send({error: false, error_type: "", err_msg: "OK!", data: result[0].dataValues});
        }
    }).catch(err => {
            res.send({
                error: true,
                error_type: err.name,
                err_msg: err.message
            });
        }
    );
});

router.get('/:source_id/', function (req, res) {

    var source_id = req.params.source_id;

    if (!source_id) {
        res.status(400);
        res.send({Bullshit: true});
    }

    models.data.findAll({
        where: {
            sourceId: source_id
        }
    }).then(result => {

        if ((result.length) === 0) {
            res.send({error: false, error_type: "", err_msg: "OK!", data: [{}]});
        } else {
            //should be only one
            res.send({error: false, error_type: "", err_msg: "OK!", data: result});
        }
    }).catch(err => {
            res.send({
                error: true,
                error_type: err.name,
                err_msg: err.message
            });
        }
    );
});


router.delete('/:source_id/:data_id', function (req, res) {

    var source_id = req.params.source_id;
    var data_id = req.params.data_id;

    if (!source_id || !data_id) {
        res.status(400);
        res.send({Bullshit: true});
    }

    models.data.destroy({
        where: {
            sourceId: source_id, id: data_id
        }
    }).then(function () {
        res.status(200);
        res.send("");

    }).catch(err => {
            res.status(404);
            res.send({
                error: true,
                error_type: err.name,
                err_msg: err.message
            });
        }
    );
});


module.exports = router;
