var models = require('../models');
var express = require('express');
var router = express.Router();

router.post('/', function (req, res) {

    var description = req.body.description;
    var name = req.body.name;


    if (!description || !name) {
        res.status(400);
        res.send({Bullshit: true});
        return
    }

    models.sources.create({
        description: description,
        name: name

    }).then(function () {
        res.send({error: false, error_type: "", err_msg: "OK!"});
    }).catch(err => {
        res.status(400);
        res.send({error: true, error_type: err.name, err_msg: err.message});
        }
    );

});


router.get('/:source_id', function (req, res) {

    var source_id = req.params.source_id;

    if (!source_id) {
        res.status(400);
        res.send({Bullshit: true});
    }

    models.sources.findAll({
        where: {
            id: source_id
        }
    }).then(result => {

        if ((result.length) === 0) {
            res.send({error: false, error_type: "", err_msg: "OK!", data: [{}]});
        } else {
            //should be only one
            res.send({error: false, error_type: "", err_msg: "OK!", data: result[0].dataValues});
        }
    }).catch(err => {
        res.status(400);
        res.send({
                error: true,
                error_type: err.name,
                err_msg: err.message
            });
        }
    );
});

router.get('/', function (req, res) {


    models.sources.findAll({}).then(result => {
        res.send({error: false, error_type: "", err_msg: "OK!", data: result});


    }).catch(err => {
        res.status(400);
            res.send({
                error: true,
                error_type: err.name,
                err_msg: err.message
            });
        }
    );
});


router.delete('/:source_id', function (req, res) {

    var source_id = req.params.user_id;

    if (!source_id) {
        res.status(400);
        res.send({Bullshit: true});
    }

    models.transactions.destroy({
        where: {
            id: source_id
        }
    }).then(function () {
        res.status(200);
        res.send("");

    }).catch(err => {
            res.status(404);
            res.send({
                error: true,
                error_type: err.name,
                err_msg: err.message
            });
        }
    );
});

module.exports = router;
